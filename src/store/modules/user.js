import storage from 'store'
import {
  login,
  getInfo,
  logout
} from '@/api/login'
import {
  getSiteDropdown , setDefault
} from '@/api/site/site'
import {
  ACCESS_TOKEN,
  DEFAULT_SITE
} from '@/store/mutation-types'
import {
  welcome
} from '@/utils/util'

const user = {
  state: {
    token: '',
    name: '',
    welcome: '',
    avatar: '',
    roles: [],
    info: {},
    sites:[]
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_NAME: (state, {
      name,
      welcome
    }) => {
      state.name = name
      state.welcome = welcome
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    },
    SET_INFO: (state, info) => {
      state.info = info
    },
    SET_SITES: (state, sites) => {
      state.sites = sites
    }
  },

  actions: {
    // 登录
    Login({
      commit
    }, userInfo) {
      return new Promise((resolve, reject) => {
        login(userInfo).then(response => {
          if (response.code === 0) {
            const result = response.res
            storage.set(ACCESS_TOKEN, result.access_token, result.expires_in)
            commit('SET_TOKEN', result.access_token)
            resolve()
          }
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取用户信息
    GetInfo({
      commit
    }) {
      return new Promise((resolve, reject) => {
        getInfo().then(response => {
          const legal = (response.code === 0 && response.res && response.res.permissions.length>0)
          if (legal) {
            let result = response.res
            commit('SET_ROLES', result.permissions)
            commit('SET_INFO', result)
            commit('SET_NAME', {
              name: result.name,
              welcome: welcome()
            })
            commit('SET_AVATAR', result.avatar)
            if(result[DEFAULT_SITE]){
               storage.set(DEFAULT_SITE, result[DEFAULT_SITE])
            }
            resolve(response)
          } else {
            setTimeout(() => {
               reject('系统检查权限不存在,禁止登陆!')
            },2000)
          }
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 加载站点
    SiteDropdown({
      commit
    }, response) {
      return new Promise((resolve, reject) => {
        if (response.code === 0) {
          getSiteDropdown({}).then(response => {
             if(response.code ===0 ){
                 commit('SET_SITES', response.res)
             }
          }).catch(() => {
             commit('SET_SITES', [])
            resolve()
          }).finally(() => {
           
          })
        }
      })
    },
    
    // 设置默认站点
    SetDefaultSite({
      commit
    }, site) {
      return new Promise((resolve, reject) => {
        if (site) {
          setDefault({siteId: site.id}).then(response => {
             if(response.code ===0 ){
                storage.set(DEFAULT_SITE, siteId)
             }
          }).catch(() => {
            resolve()
          }).finally(() => {
           
          })
        }
      })
    },
    // 登出
    Logout({
      commit,
      state
    }) {
      return new Promise((resolve) => {
        logout(state.token).then(() => {
          resolve()
        }).catch(() => {
          resolve()
        }).finally(() => {
          commit('SET_TOKEN', '')
          commit('SET_ROLES', [])
          storage.remove(ACCESS_TOKEN)
          storage.remove(DEFAULT_SITE)
        })
      })
    }

  }
}

export default user
