/* eslint-disable */
export const printANSI = () => {
  // console.clear()
  console.log('[Antd Pro] created()')
  // ASCII - ANSI Shadow
  let text = `
  _____ _     _       _    ___ _    ____ __  __ ____   __     ______  
 |_   _| |__ (_)_ __ | | _|_ _| |_ / ___|  \/  / ___|  \ \   / |___ \ 
   | | | '_ \| | '_ \| |/ /| || __| |   | |\/| \___ \   \ \ / /  __) |
   | | | | | | | | | |   < | || |_| |___| |  | |___) |   \ V /  / __/ 
   |_| |_| |_|_|_| |_|_|\_|___|\__|\____|_|  |_|____/     \_/  |_____|

\t\t\t\t\tPublished ${APP_VERSION}-${GIT_HASH} @ antdv.com
\t\t\t\t\tBuild date: ${BUILD_DATE}`
  console.log(`%c${text}`, 'color: #fc4d50')
  console.log(`%cQQ群:313095864`, 'color: #fc4d50')
  console.log('%c感谢使用 ThinkItCMS V2!', 'color: #fff; font-size: 14px; font-weight: 300; text-shadow:#000 1px 0 0,#000 0 1px 0,#000 -1px 0 0,#000 0 -1px 0;')
  console.log('%c ThinkItCMS Project Base On  Antd Vue Pro, Thanks for Antd Vue Pro!', 'color: #fff; font-size: 14px; font-weight: 300; text-shadow:#000 1px 0 0,#000 0 1px 0,#000 -1px 0 0,#000 0 -1px 0;')
}
