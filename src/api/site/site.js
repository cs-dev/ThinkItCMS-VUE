import { axios } from '@/utils/request'
import {servers} from '@/api/servers'
const serverName=servers.cms;
const api = {
    page: serverName+'/site/page',
    save: serverName+'/site/save',
    getById:serverName+'/site/getByPk',
    update: serverName+'/site/update',
    delByPk:serverName+'/site/deleteByPk',
    setDefault:serverName+'/site/setDefault',
    setOrgDefault: serverName+'/site/setOrgDefault',
    getDefault:serverName+'/site/getDefault',
    getAllSite:serverName+'/site/list',
    getSiteDropdown:serverName+'/site/getAllSite',
    loadTreeData: serverName+'/site/getOrgTres'
}
export default api

export function page (parameter) {
  return axios({
    url: api.page,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function getAllSite (parameter) {
  return axios({
    url: api.getAllSite,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}


export function getSiteDropdown (parameter) {
  return axios({
    url: api.getSiteDropdown,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}




export function save (parameter) {
  return axios({
    url: api.save,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function update (parameter) {
  return axios({
    url: api.update,
    method: 'put',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}



export function setOrgDefault (parameter) {
  return axios({
    url: api.setOrgDefault,
    method: 'put',
    params: parameter
  })
}

export function setDefault (parameter) {
  return axios({
    url: api.setDefault,
    method: 'put',
    params: parameter
  })
}

export function getDefault (parameter) {
  return axios({
    url: api.getDefault,
    method: 'get',
    params: parameter
  })
}



export function getById (parameter) {
  return axios({
    url: api.getById,
    method: 'get',
    params: parameter
  })
}


export function pageOrg (parameter) {
  return axios({
    url: api.loadTreeData,
    method: 'get',
    params: parameter
  })
}




export function delByPk(parameter) {
  return axios({
    url: api.delByPk,
    method: 'delete',
    params: parameter
  })
}



