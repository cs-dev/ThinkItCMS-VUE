import { axios } from '@/utils/request'
import {servers} from '@/api/servers'
const serverName=servers.cms;
const api = {
    page: serverName+'/webstatic/page',
    deleteFile:serverName+'/webstatic/deleteFile',
    mkdirFolder: serverName+'/webstatic/mkdirFolder',
	  downZip:serverName+'/webstatic/downZip',
	  getFileContent: serverName+'/webstatic/getFileContent',
    setFileContent: serverName+'/webstatic/setFileContent',
    uploadFile: serverName+'/webstatic/uploadFile'
}
export default api

export function page (parameter) {
  return axios({
    url: api.page,
    method: 'get',
    params: parameter
  })
}

export function downZip (parameter) {
  return axios({
    url: api.downZip,
    method: 'post',
    responseType: 'blob', 
    params: parameter
  })
}

export function getFileContent (parameter) {
  return axios({
    url: api.getFileContent,
    method: 'get',
    params: parameter
  })
}

export function setFileContent (parameter) {
  return axios({
    url: api.setFileContent,
    method: 'post',
    data: parameter
  })
}

export function uploadFile (parameter) {
  return axios({
    url: api.uploadFile,
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    data: parameter
  })
}


export function mkdirFolder (parameter) {
  return axios({
    url: api.mkdirFolder,
    method: 'put',
    params: parameter
  })
}


export function deleteFile(parameter) {
  return axios({
    url: api.deleteFile,
    method: 'delete',
    params: parameter
  })
}



