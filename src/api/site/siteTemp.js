import { axios } from '@/utils/request'
import {servers} from '@/api/servers'
const serverName=servers.cms;
const api = {
    inunstall: serverName+'/siteTemplate/inunstall',
 //    save: serverName+'/save',
	// getById:serverName+'/get',
	// update: serverName+'/update',
	// delByPk:serverName+'/delete',
}
export default api

export function inunstall (parameter) {
  return axios({
    url: api.inunstall,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

// export function save (parameter) {
//   return axios({
//     url: api.save,
//     method: 'post',
// 		headers: {
// 		  'Content-Type': 'application/json;charset=UTF-8'
// 		},
//     data: parameter
//   })
// }

// export function update (parameter) {
//   return axios({
//     url: api.update,
//     method: 'put',
// 		headers: {
// 		  'Content-Type': 'application/json;charset=UTF-8'
// 		},
//     data: parameter
//   })
// }

// export function getById (parameter) {
//   return axios({
//     url: api.getById,
//     method: 'get',
//     params: parameter
//   })
// }


// export function delByPk(parameter) {
//   return axios({
//     url: api.delByPk,
//     method: 'delete',
//     params: parameter
//   })
// }



