import { axios } from '@/utils/request'
import {
  servers
} from '@/api/servers'
const serverName = servers.cms;
const api = {
  page: serverName+'/resource/page',
  pageMedia: serverName+'/resource/listGroupPage',
	deleteFile: serverName+'/upload/deleteFile',
  listGroup: serverName+'/sysFileGroup/list',
  addGroup:serverName+'/sysFileGroup/addGroup',
  deleteGroup: serverName+'/sysFileGroup/deleteGroup',
  updateFileGid : serverName+'/sysFileGroup/updateFileGid',
  renameGroup : serverName+'/sysFileGroup/renameGroup',
  downFile:serverName+'/resource/downFile'
  
}
export default api

export function page (parameter) {
  return axios({
    url: api.page,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function downFile (parameter) {
  return axios({
    url: api.downFile,
    method: 'post',
    responseType: 'blob', 
    params: parameter
  })
}

export function pageMedia (parameter) {
  return axios({
    url: api.pageMedia,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function deleteGroup (parameter) {
  return axios({
    url: api.deleteGroup,
    method: 'delete',
    params: parameter
  })
}



export function addGroup (parameter) {
  return axios({
    url: api.addGroup,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}



export function listGroup (parameter) {
  return axios({
    url: api.listGroup,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function updateFileGid (parameter) {
  return axios({
    url: api.updateFileGid,
    method: 'post',
    params: parameter
  })
}

export function renameGroup (parameter) {
  return axios({
    url: api.renameGroup,
    method: 'post',
    data: parameter
  })
}




export function deleteFile(parameter) {
  return axios({
    url: api.deleteFile,
    method: 'delete',
    params: parameter
  })
}


