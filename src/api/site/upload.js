import { axios } from '@/utils/request'
import {servers} from '@/api/servers'
const serverName=servers.cms;
const api = {
    upload: process.env.VUE_APP_API_BASE_URL+serverName+'/upload/uploadFile',
    keepUploadFile: process.env.VUE_APP_API_BASE_URL+serverName+'/upload/keepUploadFile',
    deleteFile: serverName+'/upload/deleteFile',
}
export default api

export function upload (parameter) {
  return axios({
    url: api.upload,
    method: 'post',
    data: parameter
  })
}

export function deleteFile (parameter) {
  return axios({
    url: api.deleteFile,
    method: 'delete',
    params: parameter
  })
}


export {
  api
}



