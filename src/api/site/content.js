import { axios } from '@/utils/request'
import {servers} from '@/api/servers'
const serverName=servers.cms;
const api = {
    page: serverName+'/content/page',
    pageRecycler: serverName+'/content/pageRecycler',
    save: serverName+'/content/save',
	  getDetail:serverName+'/content/getDetail',
	  update: serverName+'/content/update',
	  delByPk:serverName+'/content/deleteByPk',
    delRealByPks:serverName+'/content/delRealByPks',
    recyclerByPks: serverName+'/content/recyclerByPks',
    delByPks: serverName+'/content/deleteByIds',
    treeCategory: serverName+'/content/treeCategory',
    getFormDesign:  serverName+'/content/getFormDesign',
    top:  serverName+'/content/top',
    getTopTag:  serverName+'/content/getTopTag',
    publish: serverName+'/content/publish',
    listRelated: serverName+'/contentRelated/listRelated',
    saveRelatedBath: serverName+'/contentRelated/saveRelatedBath',
    deleteRelated: serverName+'/contentRelated/deleteRelated',
    jobPublish: serverName+'/job/jobPublish',
    reStaticBatchGenCid: serverName+'/content/reStaticBatchGenCid',
    viewContent: serverName+'/content/viewContent',
    moveToCategory: serverName+'/content/move',
    analysisMonthTop: '/content/analysisMonthTop',
    wechatArticle:'/content/wechatArticle'
}
export default api

export function page (parameter) {
  return axios({
    url: api.page,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function pageRecycler (parameter) {
  return axios({
    url: api.pageRecycler,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function reStaticBatchGenCid(parameter) {
  return axios({
    url: api.reStaticBatchGenCid,
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    data: parameter
  })
}

export function jobPublish(parameter) {
  return axios({
    url: api.jobPublish,
    method: 'put',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    data: parameter
  })
}

export function treeCategory (parameter) {
  return axios({
    url: api.treeCategory,
    method: 'get',
    params: parameter
  })
}

export function wechatArticle (parameter) {
  return axios({
    url: api.wechatArticle,
    method: 'get',
    params: parameter
  })
}



export function viewContent (parameter) {
  return axios({
    url: api.viewContent,
    method: 'get',
    params: parameter
  })
}



export function getFormDesign (parameter) {
  return axios({
    url: api.getFormDesign,
    method: 'get',
    params: parameter
  })
}

export function getTopTag (parameter) {
  return axios({
    url: api.getTopTag,
    method: 'get',
    params: parameter
  })
}

export function analysisMonthTop (parameter) {
  return axios({
    url: api.analysisMonthTop,
    method: 'get',
    params: parameter
  })
}


export function saveRelatedBath (parameter) {
  return axios({
    url: api.saveRelatedBath,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function save (parameter) {
  return axios({
    url: api.save,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function topIt (parameter) {
  return axios({
    url: api.top,
    method: 'put',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function moveToCategory (parameter) {
  return axios({
    url: api.moveToCategory,
    method: 'put',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function update (parameter) {
  return axios({
    url: api.update,
    method: 'put',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function getDetail (parameter) {
  return axios({
    url: api.getDetail,
    method: 'get',
    params: parameter
  })
}

export function listRelated (parameter) {
  return axios({
    url: api.listRelated,
    method: 'get',
    params: parameter
  })
}



export function delByPk(parameter) {
  return axios({
    url: api.delByPk,
    method: 'delete',
    params: parameter
  })
}



export function delRealByPks(parameter) {
  return axios({
    url: api.delRealByPks,
    method: 'delete',
    data: parameter
  })
}

export function recyclerByPks(parameter) {
  return axios({
    url: api.recyclerByPks,
    method: 'put',
    data: parameter
  })
}


export function delByPks(parameter) {
  return axios({
    url: api.delByPks,
    method: 'delete',
    data: parameter
  })
}

export function deleteRelated(parameter) {
  return axios({
    url: api.deleteRelated,
    method: 'delete',
    params: parameter
  })
}


export function publish(parameter) {
  return axios({
    url: api.publish,
    method: 'put',
    data: parameter
  })
}





export function getCookie (url) {
  alert(url)
  axios.get(url,{headers:{
    host: 'www.toutiao.com',
    referer: 'https://www.toutiao.com/c/user/token/MS4wLjABAAAA6w9X2cQf213Qln-gH7oXo2gRP01u5gllJYK3ycPssaDHj1Nz4cDIAQCd8kbENKvL/?source=weitoutiao_detail&tab=article'
  }}).then(response=>{
      alert(555)
  })
}